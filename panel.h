/* slimski - Simple Login Manager
   Copyright (C) 1997, 1998 Per Liden
   Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
   Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/

#ifndef _PANEL_H_
#define _PANEL_H_

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xft/Xft.h>
#include <X11/cursorfont.h>
#include <X11/Xmu/WinUtil.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <string>

#include "switchuser.h"
#include "log.h"
#include "image.h"

class Panel {
public:
    enum ActionType {
        Login,
        Reboot,
        Halt,
        Exit,
        Suspend
    };
    enum FieldType {
        Get_Name,
        Get_Passwd
    };

    Panel(Display* dpy, int scr, Window woot, Cfg* config,  const std::string& themedir);
    ~Panel();
    void OpenPanel(const string& themedir);
    void ClosePanel();
    void ClearPanel();
    void Message(const std::string& text, int duration = 2);
    void EventHandler(const FieldType& curfield);
    string getSessiontype();   //  called from app.cpp
    bool IsCleanString(const string& s);
    void readLastUsed(string lufile);

    void PaintTextline(string& dastring, string& msg_x, string& msg_y, string& wantedfont, XftColor *wantedcolor);
    void PaintRootwinTexts();
    void showMuMsg(string& bess, int duration=2);

    ActionType getAction(void) const;

    void ResetName(void);
    void ResetPasswd(void);
    void SetName(const std::string& name);
    const std::string& GetName(void) const;
    const std::string& GetPasswd(void) const;

private:
    Panel();
    void Cursor(int visible);
    unsigned long GetColor(const char* colorname);
    void OnExpose(void);
    bool OnKeyPress(XEvent& event);
    void ShowText();
    void ChangeSessiontype();  // noted: in v1.3.6 this fn is not private

    void SlimDrawString8(XftDraw *d, XftColor *color, XftFont *font, int x, int y, const std::string& str);

    Cfg* cfg;

    // Private data
    Window Win;
    Window Rootwin;
    Display* Dpy;
    int Scr;
    int X, Y;
    GC TextGC;
    XftFont* font;
    XftColor inputcolor;
    XftColor msgcolor;
    XftFont* msgfont;
    XftFont* welcomefont;
    XftColor welcomecolor;   //  howdy   these varnames are sufficiently mnemonic ?
    XftFont* forbiddenfont;
    XftColor forbiddencolor;
    XftFont* F1font;
    XftFont* F2font;
    XftFont* F3font;
    XftFont* F4font;
    XftFont* F5font;
    XftFont* F6font;
    XftFont* F7font;
    XftFont* F8font;
    XftFont* F9font;
    XftFont* F10font;
    XftFont* F11font;
    XftFont* F12font;

    XftColor F1color;
    XftColor F2color;
    XftColor F3color;
    XftColor F4color;
    XftColor F5color;
    XftColor F6color;
    XftColor F7color;
    XftColor F8color;
    XftColor F9color;
    XftColor F10color;
    XftColor F11color;
    XftColor F12color;

    XftFont* dafont;    //    cust 1-5
    XftFont* sessionfont;
    XftColor sessioncolor;
    XftFont* enterfont;
    XftColor entercolor;
    XftColor backgroundcolor;

    XftFont* cust1font;
    XftFont* cust2font;
    XftFont* cust3font;
    XftFont* cust4font;
    XftFont* cust5font;

    XftColor cust1color;
    XftColor cust2color;
    XftColor cust3color;
    XftColor cust4color;
    XftColor cust5color;

    ActionType action;
    FieldType field;

    // Username/Password
    std::string NameBuffer;
    std::string PasswdBuffer;
    std::string HiddenPasswdBuffer;  // populated realtime with an appropriate quantity of asterisk characters

    // Configuration
    int input_name_x;
    int input_name_y;
    int input_pass_x;
    int input_pass_y;

    int input_cursor_height;
    int welcome_x;
    int welcome_y;

    int username_x;
    int username_y;

    int password_x;
    int password_y;

    int cust1_x, cust2_x, cust3_x, cust4_x, cust5_x;
    int cust1_y, cust2_y, cust3_y, cust4_y, cust5_y;

    std::string welcome_message;
    std::string F1_message;

    // Pixmap data
    Pixmap PanelPixmap;
    Pixmap LogoPixmap;    //////////

    Image* pimage;
    ////Image* logoimage;
    //Image* zpimage;    //  added late, then outcommented toward avoidng "white box"

    // For testing themes
    bool testing;
    std::string themedir;

    ////bool showlogo;

    // Sesssion handling (sic)      //   howdy  bub      sessionPID, or what?!?
    std::string sessiontype; //        reckon it should be "current_type" or...

    bool haschosenF1;
};

#endif
