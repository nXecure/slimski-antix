#!/usr/bin/sh

set -e

. /usr/share/debconf/confmodule

THIS_PACKAGE=slimski
DEFAULT_DISPLAY_MANAGER_FILE=/etc/X11/default-display-manager

#############  this block not utilized (boilerplate reserved for future use)
###      ##    create slimski group if none yet exists
###      if ! getent group slimski >/dev/null; then
###              addgroup --system slimski
###      fi
###
###      ##    create slimski user if none yet exists
###      if ! getent passwd slimski >/dev/null; then
###              adduser --system --ingroup slimski --home /var/lib/slimski slimski
###              usermod -c "slimski Display Manager" slimski
###              usermod -d "/var/lib/slimski"      slimski
###              usermod -g "slimski"               slimski
###              usermod -s "/usr/bin/false"        slimski
###      fi
###
###      mkdir -p /var/lib/slimski
###      chown slimski:slimski /var/lib/slimski
###      chmod 0750 /var/lib/slimski



# debconf is not a registry, so we only fiddle with the default file if it
# does not exist
if [ ! -e "$DEFAULT_DISPLAY_MANAGER_FILE" ]; then
  if db_get shared/default-x-display-manager; then
    # workaround debconf passthru bug (#379198)
    if [ -z "$RET" ]; then
      $RET="$THIS_PACKAGE"
    fi
#   if [ "$THIS_PACKAGE" != "$RET" ]; then
#     echo "    !!! ================= !!!"
#     echo "        ================= "
#     echo "        -=-"
#     echo "Please be sure to run \"dpkg --configure $RET\"."
#     echo "        ================= "
#     echo "        ================= "
#     echo "    !!! ================= !!!"
#   fi
    if db_get "$RET"/daemon_name; then
      echo "$RET" > $DEFAULT_DISPLAY_MANAGER_FILE
    fi
  fi
fi

# remove the displaced old default display manager file if it exists
if [ -e "$DEFAULT_DISPLAY_MANAGER_FILE.dpkg-tmp" ]; then
  rm "$DEFAULT_DISPLAY_MANAGER_FILE.dpkg-tmp"
fi

# debconf hangs if slimski gets started below without this
db_stop || true

# update-rc.d levels
S=05
K=01

if [ -f /sbin/runit ]; then
  if [ -d /etc/sv ] && [ ! -d "/etc/sv/slimski" ]; then
    cp -r /usr/share/runit/sv/slimski /etc/sv/
    echo "Copying to /etc/sv"
  fi
fi

if [ -f /sbin/runit ]; then    
    #remove /etc/init.d/slimski if it exists since we are using runit
  if [ -f /etc/init.d/slimski ]; then 
      rm /etc/init.d/slimski
  fi 
  if [ ! -h "/etc/service/slimski" ]; then
    ln -s /etc/sv/slimski/ /etc/service/
  fi  
else
  if [ -x /etc/init.d/slimski ]; then
    update-rc.d slimski defaults $S $K >/dev/null 2>&1
  fi
fi


if [ "$1" = "configure" ]; then
  invoke-rc.d dbus reload || true
fi

# antiX specific post-install
if [ -f /etc/lsb-release ] && [ $(grep "DISTRIB_ID=antiX" /etc/lsb-release) ]; then
  if [ ! -f /etc/slimski.local.conf ] && [ -x /usr/local/bin/desktop-session ]; then
    cp /usr/share/slimski/conf.slimski_antix /etc/slimski.local.conf
    # if desktop-session is installed, regenerate slimski sessiontypes
    if [ -x /usr/local/lib/desktop-session/desktop-session-loginmanager-update.sh ]; then
      /usr/local/lib/desktop-session/desktop-session-loginmanager-update.sh -k
    fi
  fi
fi

#DEBHELPER#

exit 0
