/* slimski - Simple Login Manager
   Copyright (C) 2004-06 Simone Rota <sip@varlock.com>
   Copyright (C) 2004-06 Johannes Winkelmann <jw@tks6.net>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
*/

#ifndef _CFG_H_
#define _CFG_H_

#include <string>
#include <map>
#include <vector>
                                     //      libc-6    allows 32char username
#define INPUT_MAXLENGTH_NAME    30   //  sysadmin may set lesser value, via input_maxlength_name
#define INPUT_MAXLENGTH_PASSWD  50

//  # d e fine CFGFILE SYSCONFDIR"/slimski.conf"

class Cfg {

public:
    Cfg();
    ~Cfg();

    bool readConf(std::string configfile);
    std::string parseOption(std::string line, std::string option);
    const std::string& getError() const;
    std::string& getOption(std::string option);
    int getIntOption(std::string option);
    //     ^---v      considered switching to the following (but why bother)
    //int getIntOption(std::string option, int defaultVal = 0);
    void printAllOptvals();
    std::string getWelcomeMessage();
    int getIntSesslistSize();
    std::string removeSpaces(std::string str);
    bool isValidSesstype(std::string str);

    static int absolutepos(const std::string& position, int max, int width);
    static int string2int(const char* string, bool* ok = 0);
    static void split(std::vector<std::string>& v, const std::string& str, char c, bool useEmpty=true);
    static std::string Trim(const std::string& s);
    std::string nextSession(std::string current);
    std::string dacurrentSessiontype();

    std::vector<std::string> sessiontypes;   //  late change, moved this to public

private:
    void fillSessAvailableList();

private:
    std::map<std::string,std::string> options;
    int currentSession;
    std::string error;

};

#endif

